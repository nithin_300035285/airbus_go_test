package main

import(
	"bitbucket.org/myntra/airbus-go"
	"bitbucket.org/myntra/airbus-go/entry"
	"bitbucket.org/myntra/airbus-go/logger"
	"fmt"
	"github.com/op/go-logging"
	"time"
)

type eventListener struct{}
type eventListenerHeaders struct{}
type eventListenerComplete struct{}

func main() {
    // Initialise the logger and set logging level
    logger.LogInit(logging.ERROR)

    var eventListenerEntities []*entry.EventListenerEntity
        eventListenerEntities = append(eventListenerEntities,
        &entry.EventListenerEntity{
            AirbusEvent: &entry.EventEntry{
                AppName: "test",
                EventName: "testevent",
            },
            AirbusEventListener: eventListener{},           // Only one of these two listeners is required, Depending on wether
            AirbusHeadersEventListener: eventListenerHeaders{},        // you are sending and receiving headers or not
            AirbusCompleteEventListener: eventListenerComplete{},  // Use this one only if you want complete msg info
            ConcurrentListeners: 10,
            AutoCommitDisable:  true,
        },
        &entry.EventListenerEntity{
            AirbusEvent: &entry.EventEntry{
                AppName: "test",
                EventName: "testevent",
            },
            AirbusEventListener: eventListener{},
            ConcurrentListeners: 10,    // Concurrently Listening to a single consumer
            AutoCommitDisable:  false,
            ConsumerCount: 4,       // Multiple Consumers
        })

    appName := "test"
    serviceUrl := "http://platformairbus.stage.myntra.com"

    config := make(map[string]interface{})
    config["auto.offset.reset"] = -1
    config["commit.interval"] = 5       // 5 sec

    shutdown, err := airbus_go.NewAirbusConsumer(appName, serviceUrl, config, eventListenerEntities)
    if err != nil {
        fmt.Println("Error:", err.Error())
        return
    }
    <-shutdown
}

func (this eventListener) OnEvent(key string, value interface{}) error {
    fmt.Printf("Value: %+v", value)
    return nil
}

func (this eventListenerHeaders) OnEvent(key string, value interface{}, headers map[string]string) error {
    fmt.Printf("Value: %+v, Headers: %+v\n", value, headers)
    return nil
}

func (this eventListenerComplete) OnEvent(key string, value interface{}, topic string, partition int32, offset int64, timestamp,
    blockTimestamp time.Time, headers map[string]string) error {
    fmt.Printf("Value: %+v, topic: %s\n", value, topic)
    return nil
}