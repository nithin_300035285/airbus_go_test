package main

import(
	"bitbucket.org/myntra/airbus-go"
	"bitbucket.org/myntra/airbus-go/entry"
	"bitbucket.org/myntra/airbus-go/logger"
	"fmt"
	"github.com/op/go-logging"

)

type listenableCallback struct {}

func main() {
    // Initialise the logger and set logging level
    logger.LogInit(logging.ERROR)

    serviceUrl := "http://platformairbus.stage.myntra.com"
    appName := "test"
    clientCallback := listenableCallback{}
    config := make(map[string]interface{})
    config["batch.size"] = 5
    config["batch.timeout"] = 100       // 100 ms
    config["request.timeout"] = 10000       // 10000 ms
	
    prod, err :=  airbus_go.NewAirbusProducer(serviceUrl, appName, config, clientCallback, false)
    if err != nil {
        fmt.Println(err)
        return
    }

    headers := make(map[string]string)
    headers["X-Hello"] = "TestHeader"

    event := &entry.EventEntry{
        AppName: "test",
        EventName: "testevent",
        Data: "hello idea. This is a test event.",
        Headers: headers,
    }

    for i := 0; i < 10; i++ {
        if err := prod.AsyncSend(event); err != nil {
            fmt.Println(err.Error())
        }
    }
    prod.Close()
}

func (listenableCallback) OnSuccess(result *entry.Result) {
    fmt.Printf("Config: %+v\n", result)
}

func (listenableCallback) OnFailure(err error, msg *entry.Result) {
    fmt.Printf("Error: %s\n", err.Error())
}
